import java.util.Scanner;
public class Hex2Bin {
public static void main(String[] args) {
Scanner input = new Scanner(System.in);
System.out.print("Enter Hexadecimal String: ");
String hex = input.next();
int[] dec = new int[hex.length()];
	int x;
	for(x=0;x<hex.length();x++)
	{
	String bit =""+hex.charAt(x);
	dec[x]=Integer.parseInt(bit,16);
	}
	System.out.print("The equivalent binary for hexadecimal '"+hex+"' is ");
	for(x=0;x<hex.length();x++)
	{
	String temp = ""+Integer.toBinaryString(dec[x]);
	while(temp.length()<4)
	{
	temp="0"+temp;
	}
	System.out.print(temp+" ");
	}
}
}